BIN = $(shell find . -type f -name '*.bdf' | sed -E 's|.*/(.*)\.bdf|bin/\1.otb|g')

PREFIX ?= $(DESTDIR)/usr
FNTDIR  = $(PREFIX)/share/fonts/

all : $(BIN)

bin/%.otb : src/%.bdf
	@mkdir -p bin
	fonttosfnt -o $@ $<

clean:
	rm -rf bin

install: all
	install -Dm644 bin/*.otb -t "$(FNTDIR)"/nubitmap

uninstall :
	@echo "uninstalling fonts..."
	rm -rf "$(FNTDIR)"/nubitmap

.PHONY : all clean install uninstall
